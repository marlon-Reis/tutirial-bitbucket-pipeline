package br.com.tutorial.api.repository;

import br.com.tutorial.api.ApiApplication;
import br.com.tutorial.api.model.Message;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;


//@SpringBootTest(classes = ApiApplication.class)
class MessageRepositoryTest {
//    @Autowired
//    private MessageRepository repository;

    @Test
    void create() {
        Message message = Message.builder().id(UUID.randomUUID()).messages("Hello").date(new Date()).build();
        Message response = message;// repository.save(message);
        assertThat(response, hasProperty("id", CoreMatchers.notNullValue()));
        assertThat(response, hasProperty("date", CoreMatchers.notNullValue()));
    }

    @Test
    void findAll() {
        //repository.save(Message.builder().messages("Hello").date(new Date()).build());
        List<Message> message = Arrays.asList(Message.builder().build());//repository.findAll();
        assertThat(message, CoreMatchers.notNullValue());
    }
}