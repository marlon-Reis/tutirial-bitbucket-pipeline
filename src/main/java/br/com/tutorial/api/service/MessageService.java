package br.com.tutorial.api.service;

import br.com.tutorial.api.model.Message;
import br.com.tutorial.api.repository.MessageRepository;
import br.com.tutorial.api.service.prototype.MessageServicePrototype;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessageService implements MessageServicePrototype {
    private final MessageRepository repository;

    @Override
    public Message create(Message message) {
        return repository.save(message);
    }

    @Override
    public List<Message> findAllMessages() {
        return repository.findAll();
    }
}
