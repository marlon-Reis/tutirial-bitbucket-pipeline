package br.com.tutorial.api.service.prototype;

import br.com.tutorial.api.model.Message;

import java.util.List;

public interface MessageServicePrototype {
    Message create(Message message);

    List<Message> findAllMessages();
}
