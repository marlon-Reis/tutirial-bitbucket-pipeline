package br.com.tutorial.api.controller;

import br.com.tutorial.api.model.Message;
import br.com.tutorial.api.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/tutorial/messages")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestController {
    private final MessageService messageService;

    @GetMapping("/test")
    public ResponseEntity<?> getTest() {
        var messages = Message.builder().messages("Hello Marlon, muito bom").date(new Date()).id(UUID.randomUUID()).build();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> get() {
        var messages = messageService.findAllMessages();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Message message) {
        var messageCreated = messageService.create(message);
        return new ResponseEntity<>(messageCreated, HttpStatus.OK);
    }
}
